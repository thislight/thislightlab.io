app = new Vue {
    el: "#id_main"
    data: {
        h: "There is nothing",
        h_from: "Time",
        projects: []
    }
    methods: {
        goDocument: (name) -> window.location.href = "/$name/doc/api/"
    }
}

$(document).ready(()->
    $.getJSON("https://sslapi.hitokoto.cn/?c=d",(data) ->
        app.h = data.hitokoto
        app.h_from = data.from
    )
    $.getJSON("/res/projects.json",(data) ->
        data.data.forEach((e) -> app.projects.push({
            name: e.name
        }))
    )
)
