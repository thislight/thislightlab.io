import "dart:io" show File;
import "dart:convert" show JSON;


main() async{
    var dataFile = new File("./DocBuiltProj.data");
    var str = await dataFile.readAsString();
    print(str);
    var lines = str.split("\n");
    Map m = {
        "data":[]
        };
    lines.forEach((String line){
        if(line.length < 2) return;
        print("Line: $line");
        var t = line.split(" ");
        m["data"].add({
            "name": t[1]
        });
        });
    var targetFile = new File("./res/projects.json");
    targetFile.writeAsString(JSON.encode(m)).then((_) => print("[OK] 'res/porjects.json' was created."));
}
