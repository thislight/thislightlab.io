# $1: Target GIT URI
# $2: Project name
# $3: Output Directory

echo "Turn to '$2'"
git clone $1
cd $2
pub get
pub get
dartdoc
mkdir $3/$2/
cp -a doc $3/$2/
rm -rf doc
git checkout develop
pub get
pub get
dartdoc
mkdir $3/$2/develop/
cp -a doc $3/$2/develop/
rm -rf doc
cd ..
echo "$1 $2 $3">>DocBuiltProj.data
